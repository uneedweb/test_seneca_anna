import React from 'react';
import ReactDOM from 'react-dom';
import CheckBoxList from './components/CheckBoxList';

import './styles/main.scss';

ReactDOM.render(
	<CheckBoxList />,
	document.querySelector('.app')
)
