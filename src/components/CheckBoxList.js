import React, { Component } from 'react'
import CheckBoxItem from './CheckBoxItem'

const data = [
    {
        name: 'temperature',
        options: ['hot', 'cold'],
        answer: 1
    },
    {
        name: 'option',
        options: ['option 1', 'option 2'],
        answer: 1
    },
    {
        name: 'isActive',
        options: ['active', 'not active'],
        answer: 1
    },
    {
        name: 'New',
        options: ['test1', 'test2'],
        answer: 1
    },
    {
        name: 'New 2',
        options: ['test1', 'test2'],
        answer: 1
    },
    {
        name: 'New3',
        options: ['test1', 'test2'],
        answer: 1
    },
    {
        name: 'New4',
        options: ['test1', 'test2'],
        answer: 1
    },
    {
        name: 'New5',
        options: ['test1', 'test2'],
        answer: 1
    }
]

export default class CheckBoxList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            dataAnswers: [],
            userAnswers: new Set(),
            isPassed: false,
            currentStyle: ''
        }
    }

    componentDidMount = () => {
        this.setState({
            dataAnswers: data
        })
    }

    onCheckOption = (e) => {
        const newUserAnswers = this.state.userAnswers
        const name = e.target.name

        if (newUserAnswers.has(name)) {
            newUserAnswers.delete(name)
        } else {
            newUserAnswers.add(name)
        }

        this.setState({userAnswers: newUserAnswers})
        this.checkAnswers()
    }

    checkAnswers = () => {
        const {dataAnswers, userAnswers} = this.state;
        const totalQuestions = dataAnswers.length;
        const correctAnswers = dataAnswers.reduce((res, item) => {
            let answeredYes = userAnswers.has(item.name);
            if ((answeredYes && item.answer) || (!answeredYes && !item.answer)) {
                res++;
            }
            return res;
        }, 0);
        const percentage = correctAnswers / totalQuestions * 100
        const isPassed = percentage === 100;
        const currentStyle = this.calculateCurrentStyle(percentage, totalQuestions)

        this.setState({
            isPassed,
            currentStyle
        })
    }

    calculateCurrentStyle = (percentage, total) => {
        let colorFrom, colorTo;
        if (percentage < 20) {
            colorFrom = 'rgba(250, 145, 97, 0.7)'
            colorTo = 'rgba(247, 59, 28, 0.7)'
        } else if (percentage < 100) {
            let baseRed = 330;
            let baseGreen = 100;
            let toFromDelta = 20;
            percentage = parseInt(percentage)
            colorFrom = `rgba(${baseRed - percentage}, ${baseGreen + percentage}, 97, 0.7)`
            colorTo = `rgba(${baseRed - percentage - toFromDelta}, ${baseGreen + percentage + toFromDelta}, 28, 0.7)`
        } else {
            colorFrom = 'rgb(71, 228, 193)'
            colorTo = 'rgb(7, 205, 221)'
        }
        return `linear-gradient(to bottom, ${colorFrom}, ${colorTo})`
    }

    render() {
        const {dataAnswers, userAnswers, isPassed, currentStyle} = this.state;

        const questions = dataAnswers.map(item => <CheckBoxItem
            key={`${item.name}-${item.answer}`}
            question={item}
            answers={userAnswers}
            onChange={this.onCheckOption}/>)

        const styles = {
            backgroundImage: currentStyle
        };

        return (
            <div className="App" style={styles}>
                <div className="questions">
                    {questions}
                </div>
                <div className="result">
                    {isPassed ? 'The answer is correct!' : 'The answer is incorrect :('}
                </div>
            </div>
        );
    }
}
